# README #

## Simplistic Prediction of 2016 Presidential Election ##
### Methodology ###
* Based on Democratic/Republican primary Polling Data. Uses primary polling results(actual raw votes/ no sampling) to compare the votes gotten by the eventual party candidates for the Democratic and Republican party. No segmentation, no sampling, just straightforward comparison of aggregates derived from the primary pollling data.


It tries to test the following two hypotheses.

##  Hypothesis 1 ##
### Hillary Voters vs Trump Voters ###

* Compares statewide results for Hillary Clinton vs Donald Trump - the eventual respective nominees. It ignores the polled data for their opponents from the respective parties. There were strong anti-Trump and anti-Hillary camps in both parties, so maybe they will cancel each other(just my opinion, the counter argument would be there were many Republican candidates with votes split among too many candidates and that would penalize Trump). In Arkansas, and Texas, Hillary polled more than Trump! Not sure if the underlying data from Washington is correct, it seems to have favored Trump in the primary!

* Result: (Winner needs 270+)

              PARTY           TOTAL                                                                                                                       
              ---------- ----------                                                                                                                       
              Hillary         390                                                                                                                       
              Trump           135   

               *+no data on District of Columbia(3 votes in Presidential Electoral College, making up the total of 538).                                                                                                                    
                    STATE                          Lead                                                            HC         DT                                 
------------------------------ ------------------------------------------------------ ---------- ----------                                 
                    Alabama                        Trump lead *61807                                          309928     371735                                 
                    Alaska                         Trump lead *7247                                               99       7346                                 
                    Arizona                        Trump lead *14219                                          235697     249916                                 
                    Arkansas                       Hillary lead *11436                                        144580     133144                                 
                    California                     Hillary lead *765751                                      1940580    1174829                                 
                    Colorado                       Hillary lead *49256                                         49256          0                                 
                    Connecticut                    Hillary lead *46708                                        170075     123367                                 
                    Delaware                       Hillary lead *13478                                         55950      42472                                 
                    Florida                        Hillary lead *20179                                       1097400    1077221                                 
                    Georgia                        Hillary lead *41301                                        543008     501707                                 
                    Hawaii                         Hillary lead *4450                                          10127       5677                                 
                    Idaho                          Trump lead *57413                                            5065      62478                                 
                    Illinois                       Hillary lead *460711                                      1012175     551464                                 
                    Indiana                        Trump lead *287078                                         303382     590460                                 
                    Iowa                           Hillary lead *24314                                         69733      45419                                 
                    Kansas                         Trump lead *4469                                            12593      17062                                 
                    Kentucky                       Hillary lead *130057                                       212550      82493                                 
                    Louisiana                      Hillary lead *96797                                        221615     124818                                 
                    Maine                          Hillary lead *1214                                           1214          0                                 
                    Maryland                       Hillary lead *296624                                       533247     236623                                 
                    Massachusetts                  Hillary lead *292471                                       603784     311313                                 
                    Michigan                       Hillary lead *93044                                        576795     483751                                 
                    Mississippi                    Trump lead *9308                                           182447     191755                                 
                    Missouri                       Trump lead *71491                                          310602     382093                                 
                    Montana                        Trump lead *58862                                           55194     114056                                 
                    Nebraska                       Trump lead *106947                                          14340     121287                                 
                    Nevada                         Trump lead *28235                                            6296      34531                                 
                    New Hampshire                  Trump lead *5154                                            95252     100406                                 
                    New Jersey                     Hillary lead *197540                                       554237     356697                                 
                    New Mexico                     Hillary lead *36921                                        110451      73530                                 
                    New York                       Hillary lead *529151                                      1054083     524932                                 
                    North Carolina                 Hillary lead *158232                                       616383     458151                                 
                    North Dakota                   Hillary lead *101                                             101          0                                 
                    Ohio                           Trump lead *48319                                          679266     727585                                 
                    Oklahoma                       Hillary lead *9197                                         139338     130141                                 
                    Oregon                         Hillary lead *10935                                        251739     240804                                 
                    Pennsylvania                   Hillary lead *25987                                        918689     892702                                 
                    Rhode Island                   Hillary lead *13434                                         52493      39059                                 
                    South Carolina                 Hillary lead *31663                                        271514     239851                                 
                    South Dakota                   Trump lead *17820                                           27046      44866                                 
                    Tennessee                      Trump lead *87398                                          245304     332702                                 
                    Texas                          Hillary lead *177462                                       935080     757618                                 
                    Utah                           Trump lead *9198                                            15666      24864                                 
                    Vermont                        Trump lead *1633                                            18335      19968                                 
                    Virginia                       Hillary lead *147398                                       503358     355960                                 
                    Washington                     Trump lead *395863                                           7140     403003                                 
                    West Virginia                  Trump lead *69891                                           86354     156245                                 
                    Wisconsin                      Hillary lead *46397                                        432767     386370                                 
                    Wyoming                        Hillary lead *54                                              124         70                                 

                    49 rows selected.

    * State Wise break-up with Electoral College Votes Data

                    STATE                          WHICH_PARTY State_Electoral_Votes:+Dem-Rep VOTES_CARDINALITY_DIFF                                            
------------------------------ ----------- ------------------------------ ----------------------                                            
                    Alabama                        Republican                              -9                 -61807                                            
                    Alaska                         Republican                              -3                  -7247                                            
                    Arizona                        Republican                             -10                 -14219                                            
                    Arkansas                       Democratic                               6                  11436                                            
                    California                     Democratic                              55                 765751                                            
                    Colorado                       Democratic                               9                  49256                                            
                    Connecticut                    Democratic                               7                  46708                                            
                    Delaware                       Democratic                               3                  13478                                            
                    Florida                        Democratic                              27                  20179                                            
                    Georgia                        Democratic                              15                  41301                                            
                    Hawaii                         Democratic                               4                   4450                                            
                    Idaho                          Republican                              -4                 -57413                                            
                    Illinois                       Democratic                              21                 460711                                            
                    Indiana                        Republican                             -11                -287078                                            
                    Iowa                           Democratic                               7                  24314                                            
                    Kansas                         Republican                              -6                  -4469                                            
                    Kentucky                       Democratic                               8                 130057                                            
                    Louisiana                      Democratic                               9                  96797                                            
                    Maine                          Democratic                               4                   1214                                            
                    Maryland                       Democratic                              10                 296624                                            
                    Massachusetts                  Democratic                              12                 292471                                            
                    Michigan                       Democratic                              17                  93044                                            
                    Mississippi                    Republican                              -6                  -9308                                            
                   Missouri                       Republican                             -11                 -71491                                            
                   Montana                        Republican                              -3                 -58862                                            
                   Nebraska                       Republican                              -5                -106947                                            
                   Nevada                         Republican                              -5                 -28235                                            
                   New Hampshire                  Republican                              -4                  -5154                                            
                   New Jersey                     Democratic                              15                 197540                                            
                   New Mexico                     Democratic                               5                  36921                                            
                   New York                       Democratic                              31                 529151                                            
                   North Carolina                 Democratic                              15                 158232                                            
                   North Dakota                   Democratic                               3                    101                                            
                   Ohio                           Republican                             -20                 -48319                                            
                   Oklahoma                       Democratic                               7                   9197                                            
                   Oregon                         Democratic                               7                  10935                                            
                   Pennsylvania                   Democratic                              21                  25987                                            
                   Rhode Island                   Democratic                               4                  13434                                            
                   South Carolina                 Democratic                               8                  31663                                            
                   South Dakota                   Republican                              -3                 -17820                                            
                   Tennessee                      Republican                             -11                 -87398                                            
                   Texas                          Democratic                              34                 177462                                            
                   Utah                           Republican                              -5                  -9198                                            
                   Vermont                        Republican                              -3                  -1633                                            
                   Virginia                       Democratic                              13                 147398                                            
                   Washington                     Republican                             -11                -395863                                            
                   West Virginia                  Republican                              -5                 -69891                                            
                   Wisconsin                      Democratic                              10                  46397                                            
                   Wyoming                        Democratic                               3                     54                                            

                   49 rows selected.

    * Full result at [Source](http://handyartisan.bitbucket.org/query.html)

## Hypothesis 2 ##
### Democrats vs Republicans ###

* Compares state wise votes polled for Democratic vs Republican party. This should account for the splitting of the votes among too many candidates drawback of hypothesis 1

             PARTY        TOTAL                                                                                                                      
             ----------- ----------                                                                                                                      
             Democrats          236                                                                                                                      
             Republicans        289    

* Full result [here](http://handyartisan.bitbucket.org/query.html)
* Code
    * [dataset](https://bitbucket.org/handyartisan/presidential-election-2016-prediction/src/c4310dc636ca76fbd1a7e9bb4e1a4ce618929b8e/data/?at=master) - dataset for primary election
    * [query/result](https://bitbucket.org/handyartisan/presidential-election-2016-prediction/src/c4310dc636ca76fbd1a7e9bb4e1a4ce618929b8e/sql/?at=master) : SQL queries that tests the hypothesis



### References ###

* Election Data:
   
    * 2016 Primary Results Data set:https://www.kaggle.com/benhamner/2016-uselection/downloads/primary_results.csv.zip 
    * Electoral College: http://mathforum.org/workshops/sum96/data.collections/datalibrary/ElectoralPopArea.xls
    * Methodology/Sample Questions typically asked in [a fox opinion poll](https://fnn.app.box.com/s/o8csymq7krna35xl38fw2kvhmt92p0wm):  Random sample of around 1000 registered voters, proportional to each state(roughly two times the electoral votes per state, say 100 for California)