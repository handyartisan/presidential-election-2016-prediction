set lines 140
set pages 1000
set echo on
spool query.log
spool query.html
set markup html on
-- column state format a30 justify right

-- Hypothesis 1
-- Primary Election. Hillary vs Trump

-- Summary
with  
pivot_data as (select state, candidate, votes from primary_results), 
pv1 as (select * from pivot_data pivot(sum(votes) for candidate in ('Hillary Clinton' as HC, 'Donald Trump'as DT ))), 
pv2 as (select state, nvl(HC,0) HC, nvl(DT,0) DT from pv1), 
pv3 as (select pv2.state, case when 1=1 then HC-DT else 0 end HC_DT, HC, DT, state_evotes 
	from pv2, electoral_college ec where ec.state = pv2.state), 
pv_state as ( select state, case when HC_DT> 0 then (1*state_evotes) else (-1*state_evotes) end dem_rep from pv3), 
pv_party as ( select state, case when dem_rep > 0 then 'Hillary' else 'Trump' end party, dem_rep from pv_state) 
select party, abs(sum(dem_rep)) total from pv_party group by party order by 1;

-- Detail
with 
pivot_data as (select state, candidate, votes from primary_results), 
pv1 as (select * from pivot_data pivot(sum(votes) for candidate in ('Hillary Clinton' as HC, 'Donald Trump'as DT ))), 
pv2 as (select state, nvl(HC,0) HC, nvl(DT,0) DT from pv1), 
pv3 as (select pv2.state, case when 1=1 then HC-DT else 0 end HC_DT, HC, DT, state_evotes 
	from pv2, electoral_college ec where ec.state = pv2.state), 
pv_state as ( select state, case when HC_DT> 0 then (1*state_evotes) else (-1*state_evotes) end dem_rep, hc_dt from pv3) 
select state, case when dem_rep > 0 then 'Hillary ' else 'Trump ' end which_party, 
	dem_rep "State_Electoral_Votes:+Dem-Rep", hc_dt votes_cardinality_diff 
from pv_state order by 1; 


-- Lead per state.
with 
pivot_data as (select state, candidate, votes from primary_results), 
pv1 as (select * from pivot_data pivot(sum(votes) for candidate in ('Hillary Clinton' as HC, 'Donald Trump'as DT ))), 
pv2 as (select state, nvl(HC,0) HC, nvl(DT,0) DT from pv1) 
select state, case when HC > DT then 'Hillary lead *' || to_char(HC- DT) else 'Trump lead *' || to_char(DT- HC) end res, HC, DT 
from pv2 order by state;



select state , party,candidate, sum(votes) from primary_results group by STATE, rollup (party, candidate) order by 1,4 desc;

create or replace view rollup_view as 
select state, party,candidate, sum(votes) votes 
from primary_results group by STATE, rollup (party, candidate) order by 1,4 desc;

select * from rollup_view where candidate is null and party is not null;

create or replace view rollup_view2 as select * from rollup_view where candidate is null and party is not null;
-- Hypothesis 2

--detail
with 
pivot2_data as (select state, party, votes from rollup_view2), 
pv1 as (select * from pivot2_data pivot(sum(votes) for party in ('Democrat' as HC, 'Republican' as DT ))), 
pv2 as (select state, nvl(HC,0) HC, nvl(DT,0) DT from pv1), 
pv3 as (select pv2.state, case when 1=1 then HC-DT else 0 end HC_DT, HC, DT, HC+DT total, 
	trunc((100*(abs(HC-DT)))/(HC+DT), 1) percentage, state_evotes 
	from pv2, electoral_college ec where ec.state = pv2.state), 
pv_state as ( select state, case when HC_DT> 0 then (1*state_evotes) else (-1*state_evotes) end dem_rep, hc_dt, total, percentage
		 from pv3) 
select state, case when dem_rep > 0 then 'Democratic ' else 'Republican ' end which_party, 
	dem_rep, hc_dt votes_cardinality_diff, total, percentage -- to_char(percentage, '9999999.99') percentage
from pv_state order by 1;

-- summary

with 
pivot2_data as (select state, party, votes from rollup_view2), 
pv1 as (select * from pivot2_data pivot(sum(votes) for party in ('Democrat' as HC, 'Republican' as DT ))), 
pv2 as (select state, nvl(HC,0) HC, nvl(DT,0) DT from pv1), 
pv3 as (select pv2.state, case when 1=1 then HC-DT else 0 end HC_DT, HC, DT, state_evotes 
	from pv2, electoral_college ec where ec.state = pv2.state), 
pv_state as ( select state, case when HC_DT> 0 then (1*state_evotes) else (-1*state_evotes) end dem_rep, hc_dt from pv3),
pv_party as (select state, case when dem_rep > 0 then 'Democrat ' else 'Republican ' end which_party, dem_rep, hc_dt votes_cardinality_diff from pv_state)
select  which_party, abs(sum(dem_rep)) total from pv_party group by which_party order by 1;
spool off
exit;
