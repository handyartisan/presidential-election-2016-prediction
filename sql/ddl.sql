connect scott/tiger
create or replace directory MY_DATA as '/scratch/cunnitha/code/election-analysis/data';

drop table primary_results;
create table primary_results
(
state	varchar2(30),
state_abbreviation varchar2(30),
county	varchar2(100),
fips	number,
party	varchar2(30),
candidate	varchar2(30),
votes	number,
fraction_votes  number
) 
ORGANIZATION EXTERNAL
(TYPE ORACLE_LOADER
 DEFAULT DIRECTORY my_data
ACCESS PARAMETERS
(RECORDS DELIMITED BY NEWLINE
 LOGFILE my_data:'data.log'
 BADFILE my_data:'data.bad'
 DISCARDFILE my_data:'data.disc'
 fields terminated by ','
 MISSING FIELD VALUES ARE NULL
)
 LOCATION (my_data:'primary_results.csv')
) parallel;

select count(*) from primary_results;



drop table electoral_college;
create table electoral_college
(
state	varchar2(30),
state_evotes number
)
ORGANIZATION EXTERNAL
(TYPE ORACLE_LOADER
 DEFAULT DIRECTORY my_data
ACCESS PARAMETERS
(RECORDS DELIMITED BY NEWLINE
 LOGFILE my_data:'ec.log'
 BADFILE my_data:'ec.bad'
 DISCARDFILE my_data:'ec.disc'
 fields terminated by ','
 MISSING FIELD VALUES ARE NULL
)
 LOCATION (my_data:'electoral_college.csv')
) parallel;
